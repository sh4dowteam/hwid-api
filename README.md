# README #

Sh4dowCode - HWID API

* Download Latest Build: https://bitbucket.org/sh4dowteam/hwid-api/raw/998ce01cd83b2b6f795cffdd4491cb420d3ba80c/builds/HWID-API-v1.0.jar

### What is this repository for? ###

* A Way for Java Developers to get a HWID for everyone of their Users. 
* Version v1.0

### How do I get set up? ###

* Import the Jar File in your Java Project as Library 
* Make sure you have following Dependencies: java.io, java.net & java.security
* Import ```io.sh4dow.hwid.Main```
* Get the HWID via ```java
StringName = getHWID(salt1, salt2);
```


### Who do I talk to? ###

* Sh4dowCode (https://reddit.com/u/Sh4dowCode)