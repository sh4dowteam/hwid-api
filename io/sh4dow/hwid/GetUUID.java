package io.sh4dow.hwid;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class GetUUID {
	public static String get() {
		{
		    try {
		        Process p = Runtime.getRuntime().exec("wmic csproduct get uuid");
		        BufferedReader inn = new BufferedReader(new InputStreamReader(p.getInputStream()));

		        while (true) {

		            String line = inn.readLine();
		            if (line == null) {
		                return line;
		            }
		            return line;
		        }
		    } catch (Exception e) {
		    	return null;
		    }
		}
	}

}
