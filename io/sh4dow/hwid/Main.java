package io.sh4dow.hwid;

public class Main {
	
	protected static String PID;
	protected static String Motherboard;
	protected static String UUID;
	protected static String BIOSv;
	protected static String MAC;
	
	public static String getHWID(String salt, String salt2) {
		String HWID;
		if (!Tools.isWindows()) {
			System.err.println("[HWID API] Warning: HWID API is only designed for Windows Coputers!");
		}
		HWID = getHWIDPt2(salt, salt2);
		return HWID;
		
	}

	public static void main(String[] args) {
		PID = GetPID.get();	//Changing after switching Processor
		Motherboard = GetMtrbrd.get();//Changing after switching Motherboard
		UUID = GetUUID.get();	//Changing after switching any part of the pc
		BIOSv = GetBIOSv.get();	//Changing after BIOS Update
		MAC = GetMAC.get();	//Is Spoofable | Is changing after switching Networkcard
	}
	
	public static String getHWIDPt2(String salt, String salt2) {
		String HWID;
		String UnhashedHWID = salt + PID + Motherboard + UUID + BIOSv + MAC + salt2;
		HWID = Tools.sha256(UnhashedHWID);
		
		return HWID;
	}

}
